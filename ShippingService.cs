using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_RPPOON
{
    class ShippingService
    {
        private double a;

        public ShippingService(double a) { this.a = a; }

        public double Price(double weight)
        {
            return a * weight;
        }

        public override string ToString()
        {
            return "Cijena dostave:";
        }
    }
}