using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            //1 i 2 zad
            List<IShipable> list = new List<IShipable>();
            Box box = new Box("something");
            Product product = new Product("iPhone", 40.2, 10);
            Product best = new Product("Samsung", 10, 29);
            list.Add(box);
            
            list.Add(product);
            list.Add(best);

            ShippingService price = new ShippingService(4);
            double a = 0;
            foreach (IShipable ship in list)
            {
            
               a += ship.Weight;

                Console.WriteLine(ship.Description());

            }

            Console.WriteLine(price.ToString() + price.Price(a)+" kn");

        //3 zad
            Dataset data = new Dataset("LV5.txt");
            User person1 = User.GenerateUser("Ivan");
            User person2 = User.GenerateUser("Marija");
            User person3 = User.GenerateUser("Mihaela");
            User person4 = User.GenerateUser("Sara");
            User person5 = User.GenerateUser("Filip");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(person1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(person2);
            ProtectionProxyDataset proxy3 = new ProtectionProxyDataset(person3);
            ProtectionProxyDataset proxy4 = new ProtectionProxyDataset(person4);
            ProtectionProxyDataset proxy5 = new ProtectionProxyDataset(person5);
            VirtualProxyDataset proxy6 = new VirtualProxyDataset("LV5.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            
            printer.Print(proxy1);
            Console.WriteLine();
            
            printer.Print(proxy2);
            Console.WriteLine();
            
            printer.Print(proxy3);
            Console.WriteLine();
            
            printer.Print(proxy4);
            Console.WriteLine();
            
            printer.Print(proxy5);
            Console.WriteLine();
            
            Console.WriteLine("\n");
            printer.Print(proxy6);
        }
    }
}